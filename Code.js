/**
 *
 */
var SIDEBAR_TITLE = 'Редактирование дока';
var DOC_NAME = 'spreadsheet-#sidebar-CRUD-@form';
var DEF_TAB_NAME = '';

//        server functions triggers


/**
 * Adds a custom menu with items to show the sidebar and dialog.
 *
 * @param {Object} e The event parameter for a simple onOpen trigger.
 */
function onOpen(e) {

  SpreadsheetApp.getUi()
    //.createAddonMenu()
    .createMenu('НАЧАЛО РАБОТЫ!')
    .addItem('СТАРТ!', 'showSidebar')
    .addToUi()
}


/**
 * Runs when the add-on is installed; calls onOpen() to ensure menu creation and
 * any other initializion work is done immediately.
 *
 * @param {Object} e The event parameter for a simple onInstall trigger.
 */
function onInstall(e) {

  createChangeTrigger()
  createEditTrigger()
  onOpen(e)
}


/**
  *
  */
function createChangeTrigger() {

  var spreadsheet = SpreadsheetApp.getActive();
  ScriptApp.newTrigger('onChange2')
  .forSpreadsheet(spreadsheet)
  .onChange()
  .create()
}


/**
  *
  */
function createEditTrigger() {

  var spreadsheet = SpreadsheetApp.getActive();
  ScriptApp.newTrigger('onEdit2')
  .forSpreadsheet(spreadsheet)
  .onEdit()
  .create()
}


/**
  *
  */
function onChange2(e) {

  showSidebar()
  console.log('>>onChange()')
}


/**
  *
  */
function onEdit2(e) {
 // var res = UrlFetchApp.fetch("http://numbersapi.com/random/math");
 // Logger.log('res.getContentText() = ', res.getContentText())

  showSidebar()
  console.log('>>onEdit()')
}


/**
  *
  */
function onSelectionChange(e) {

  var range = e && e.range;
  var rowNum;
  Logger.log('>>e = ', e)

  if (range){
    rowNum = range.getRow()
    Logger.log('>>rowNum = ', rowNum)
    hlpGetValCached(null, {tabNum: -1, rowNum: 1}, true)
    hlpGetValCached(null, {tabNum: -1, rowNum: -1}, true)
  }

}


//        server functions - ui, sys

/**
 * Opens a sidebar. The sidebar structure is described in the Sidebar.html
 * project file.
 */
function showSidebar() {

  var ui = HtmlService.createTemplateFromFile('Sidebar')
    .evaluate()
   // .setSandboxMode(HtmlService.SandboxMode.IFRAME)
    .setTitle(SIDEBAR_TITLE);

  SpreadsheetApp.getUi()
    .showSidebar(ui)
}


/**
 * Refresh authorization in docs.google.com
 * if needed
 */
function getOAuthToken() {

 DriveApp.getRootFolder();
 return ScriptApp.getOAuthToken();

}


//        helpers functions

/**
 *
 */

function hlpGetSpreadsheetSheets() {

  var sheets = SpreadsheetApp.getActiveSpreadsheet()
    .getSheets();
  var result = [];
  console.log('sheets = ', sheets)
  for (var i = 0; i < sheets.length; i ++) {
    result[i] = sheets[i].getName()
  }
  console.log('result = ', result)
  return result
}

/**
 *
 */
function hlpGetSheetRow(tabName, rowNum) {

  var sheet = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(tabName)
    || SpreadsheetApp.getActiveSheet();

  var data = sheet.getDataRange()
    .getValues();


  //Logger.log('data = ', data)
  if (rowNum < 0)
    rowNum = sheet.getActiveCell()
      .getRow()

  if (rowNum == 0)
    rowNum = 1

  Logger.log('rowNum2 = ', rowNum)

  if (data[rowNum-1] !== undefined)
    return data[rowNum-1]

  return []
}


/**
 *
 */
function hlpSetSheetRow(tabName, rowNum, recordArr) {

  Logger.log('recordArr = ', recordArr)
  var sheet = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(tabName)
    || SpreadsheetApp.getActiveSheet();


  //Logger.log('data = ', data)
  if (rowNum <= 0)
    rowNum = sheet.getActiveCell()
      .getRow()

  sheet.getRange(rowNum, 1, 1, recordArr.length)
    .setValues([recordArr])

}


/**
 *
 */
function hlpSearchSheetCell(tabName, colNum, colVal){

  Logger.log('colNum = ', colNum, ' colVal = ', colVal)
  var sheet = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(tabName)
    || SpreadsheetApp.getActiveSheet();

  var data = sheet.getDataRange()
    .getValues();

  var rowNum = 0;

  Logger.log('data = ', data)

  //var rowData = [];

  if (colNum) {

    rowNum = data.map(function(item, index){
      //search from index = 1
      return index
      ? (String(item[colNum - 1]).indexOf(colVal) > -1
        ? index + 1 // needs rowNum = [0, ...)
        : -1
      )
      : -1

    })

    Logger.log('data[x][colNum - 1] = ', rowNum)
    rowNum = rowNum
    .find(function(item){
      return item > -1
    })
    || 0
  }
  Logger.log('rowNum = ', rowNum)

  if ( colNum && rowNum ) {

    var resRange = sheet.getRange(rowNum, colNum, 1, sheet.getMaxColumns() - colNum)
    Logger.log('resRange=', resRange)

    if (resRange) {
      //sheet.setActiveSelection(resRange)
      sheet.setActiveRange(resRange)
    }
  }

}

/**
 *
 */
function hlpGetValCached(valName, valParams, invalidate, noCache) {

  Logger.log('valName = ', valName, ' valParams = ', valParams,
    ' invalidate = ', invalidate, ' noCache = ', noCache)
  //return ['test1', 'test2']

  var dataName = valName;
  var data = null;

  if (valParams
        && valParams.tabNum
        && valParams.rowNum){

        valName = null
        dataName = `${DOC_NAME}#${valParams.tabNum}!${valParams.rowNum}:${valParams.rowNum}`
      }
  else if (! valName)
    return null

  Logger.log('dataName = ', dataName)



  if (! invalidate
    && ! noCache) {

      data = CacheService.getScriptCache()
        .get(dataName)

      try{
        data = JSON.parse(data)
      }
      catch(e){
        data = null
      }

  }

  console.log('get cache data = ', data)

  if (! data
    || invalidate
    || noCache) {

    if (! valName)
      data = hlpGetSheetRow(valParams.tabNum, valParams.rowNum)
    else
      data = hlpGetSpreadsheetSheets()

      console.log('data = ', data)

    if (! noCache) {

      CacheService.getScriptCache()
        .put(dataName, JSON.stringify(data))
      }

  }

  return data

}

/**
 *
 */
function hlpGetSheetRowCached(tabName, rowNum, invalidate, noCache){

  // @todo tabName => tabNum

  var dataName = `${DOC_NAME}#${tabName}!${rowNum}:${rowNum}`; //''+rowNum;
  Logger.log('>>rowNum = ', rowNum,
    ' invalidate = ', invalidate,
    ' dataName = ', dataName)
  var data = null;

  if (invalidate){

      data = hlpGetSheetRow(tabName, rowNum)
      CacheService.getScriptCache()
        .put(dataName, JSON.stringify(data))
      Logger.log('>>getScriptCache.put > ', data)
      return data
    }


  var data;
  if (! noCache)
    data = CacheService.getScriptCache()
      .get(dataName);

  Logger.log('>>getScriptCache.get < ', data)

// @todo cut this off
  try {
    data = JSON.parse(data)
  }catch(e){
    ;
  };Logger.log('>>data2 = ', data)

  if (! data || noCache){

    data = hlpGetSheetRow(tabName, rowNum)
    if (! noCache)
      CacheService.getScriptCache()
        .put(dataName, JSON.stringify(data))
  };Logger.log('>>data3 = ', data)

  if (! data)
    throw ('ERR: data empty')

  return data
}


//        server user functions (CRUD, etc)


/**
 *
 */

 function srvGetSpreadsheetSheets(){

   return JSON.stringify(
     hlpGetValCached(`${DOC_NAME}`, null)
   )
 }

/**
 * Returns the active row.
 *
 * @return {Object[]} The headers & values of all cells in row.
 */
function srvGetSheetRow(sheet, row, timeout) {

  var no_Client_Invalidation = CacheService.getScriptCache()
    .get('no_Client_Invalidation');
  Logger.log('no_Client_Invalidation = ', no_Client_Invalidation)

  // @todo
  var active_Sheet = SpreadsheetApp.getActiveSpreadsheet()
    .getActiveSheet()
    .getName()

  // keep tab, headData, rowData
  var tab = sheet;//DEF_TAB_NAME; // @todo for getting by tab name

  var headRow = hlpGetValCached(null, {tabNum: tab, rowNum: 1});
  //hlpGetSheetRowCached(tab, 1);
  var currRow = hlpGetValCached(null, {tabNum: tab, rowNum: row}); //current
  Logger.log('>>objRecords(); headRow = ', headRow)
  Logger.log('>>objRecords(); currRow = ', currRow)
  var objRecords = [['0', active_Sheet]];
  var idx = 1;
  headRow.map(function(item, index){
      var key = headRow[index].trim().length
        ? headRow[index].trim()
        : `COL${index + 1}`
      //Logger.log('index = ', index)
      objRecords.push([
        // @not works [index]: currRow && currRow[num] || ''
        key,
        currRow && currRow[index] || ''
      ])
    })
    // add invalidation status
    objRecords.push(['', no_Client_Invalidation])

    CacheService.getScriptCache()
      .put('no_Client_Invalidation', false)

    Logger.log('>>objRecords = ', objRecords)
  return JSON.stringify(objRecords)

}

/**
 *
 */
function srvSetSheetRow(record) {

  Logger.log('record = ', record)
  var arr = record.map(function(item, num){
    return item[1] || ''
  })

  hlpSetSheetRow('', -1, arr)
}


/**
 *
 */
 function srvSearchSheetRow(record) {

   Logger.log('record = ', record)
   // find first nonempty pair header: value
   // or first item
   var arr = record.find(function(item){
     return item[1].trim().length
   }) || record[0];
   //
   Logger.log('arr = ', arr)
   //colNum really must be > 0
   var colNum = record.map(function(item, index){
       return item[0] == arr[0]
       ? index + 1 // needs to have colNum = (0, ...]
       : -1
     })
     .filter(function(item){
       return item > -1
     }).pop()
     || parseInt(arr[0].replace(/^COL/, ''))
     || 0

   Logger.log('colNum = ', colNum)

   if (colNum) {

     var no_Client_Invalidation = CacheService.getScriptCache()
      .put('no_Client_Invalidation', true)

     hlpSearchSheetCell('', colNum, arr[1])
   }
 }


 function srvSetSpreadsheetSheet(sheetName) {

  var sheet = SpreadsheetApp.getActive()
    .getSheetByName(sheetName);
  if (sheet)
    SpreadsheetApp.setActiveSheet(sheet)
 }
